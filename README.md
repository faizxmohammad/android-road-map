# Android road map
This is a beginners guide for Android development. This road map covers the steps and resources which are needed when starting with android development.


## Getting started

To make it easy for you to get started with Android, here's a list of recommended next steps to be followed.

Step 1 : 
Choose a programming language 

We can start either with Java or Kotlin , but this roadmap focuses on Kotlin, so its better to choose  `Kotlin` as programming language.

#### Start with basics

1. Datatypes
2. Operators
3. Control flow statements 
4. Loops
5. Functions
6. Object oriented programming (OOP).

- [Kotlin Hindi Playlist](https://www.youtube.com/playlist?list=PLRKyZvuMYSIMW3-rSOGCkPlO1z_IYJy3G)
- [English Playlist](https://www.youtube.com/playlist?list=PLQkwcJG4YTCRSQikwhtoApYs9ij_Hc5Z9)
- [Udemy course](https://www.udemy.com/course/the-complete-android-kotlin-developer-course-kotlin-a-ztm/)


These are the few courses we recommended if you are a beginner, although you can have your choice of resources to learn from.


**Note**: After learning these concepts, it is recommended to make few projects before moving further, since it increases your OOP skills.


Now that you have covered all the necessary basics , we can start with our android development.

### Lets learn Android basics
The following are the basics that every android developer should be aware of and should have the knowledge of how to use it.

1. Layouts and their types.
(Since there are many we will suggest you to learn these four first)

    - Linear Layout
    - Relative Layout
    - Frame Layout
    - Table Layout
    - Constraint Layout

2. Textviews.
3. Imageviews.
4. Buttons and its types.
5. Spinner and checkboxes.
6. Toast message, Snackbar message and Dialouge message.
7. List views.
8. Recycler views.
9. Grid views.
10. Scroll views.
11. Web views.


